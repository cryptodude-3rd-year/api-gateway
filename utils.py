import datetime as dt

from grpc import insecure_channel

from config.constants import (
    USER_MANAGER_IP,
    AUTH_IP,
    DATA_STORAGE_IP,
    STATISTIC_GEN_IP,
    USER_MANAGER_PORT,
    AUTH_PORT,
    DATA_STORAGE_PORT,
    STATISTIC_GEN_PORT,
)
from generated_py_proto.user_manager.dashboard_pb2_grpc import DashboardStub
from generated_py_proto.user_manager.subscription_pb2_grpc import ExchangeSubStub, CurrencySubStub
from generated_py_proto.auth.auth_pb2_grpc import OAuthStub, AuthStub, UserManagementStub
from generated_py_proto.data_storage.data_storage_pb2_grpc import CurrenciesStub, ExchangesStub
from generated_py_proto.statistic_gen.statistic_generation_pb2_grpc import StatisticGenerationServiceStub
from generated_py_proto.user_manager.tech_support_pb2_grpc import TechSupportStub

stub_mapping = {
    "dashboard": DashboardStub,
    "exchange_sub": ExchangeSubStub,
    "currency_sub": CurrencySubStub,
    "oauth": OAuthStub,
    "auth": AuthStub,
    "user_management": UserManagementStub,
    "ds_currency": CurrenciesStub,
    "ds_exchange": ExchangesStub,
    "statistic": StatisticGenerationServiceStub,
    "tech_support": TechSupportStub,
}

port_mapping = {
    "dashboard": USER_MANAGER_PORT,
    "exchange_sub": USER_MANAGER_PORT,
    "currency_sub": USER_MANAGER_PORT,
    "tech_support": USER_MANAGER_PORT,
    "oauth": AUTH_PORT,
    "auth": AUTH_PORT,
    "user_management": AUTH_PORT,
    "ds_currency": DATA_STORAGE_PORT,
    "ds_exchange": DATA_STORAGE_PORT,
    "statistic": STATISTIC_GEN_PORT,
}
ip_mapping = {
    "dashboard": USER_MANAGER_IP,
    "exchange_sub": USER_MANAGER_IP,
    "currency_sub": USER_MANAGER_IP,
    "tech_support": USER_MANAGER_IP,
    "oauth": AUTH_IP,
    "auth": AUTH_IP,
    "user_management": AUTH_IP,
    "ds_currency": DATA_STORAGE_IP,
    "ds_exchange": DATA_STORAGE_IP,
    "statistic": STATISTIC_GEN_IP,
}


def create_stub(stub_type):
    channel = insecure_channel(f"{ip_mapping[stub_type]}:{port_mapping[stub_type]}")
    stub = stub_mapping[stub_type](channel)

    return stub


def form_repeated_field(field_data):
    return None if field_data == [0] else list(field_data)


def form_datetime_field(field_data):
    return None if field_data == 0 else dt.datetime.fromtimestamp(field_data).strftime("%d.%m.%Y %H:%M")
