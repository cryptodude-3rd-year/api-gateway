from pydantic import BaseModel, EmailStr


class AccessTokenRequest(BaseModel):
    access_token: str


class LoginRequest(BaseModel):
    email: str
    password: str


class RegisterRequest(BaseModel):
    name: str
    email: EmailStr
    password: str


class DeleteUser(BaseModel):
    id: int


class UpdateUser(BaseModel):
    id: int
    name: str
