import generated_py_proto.auth.auth_pb2 as auth_types
from serializers import serialize


def form_access_token_request_msg(data):
    msg = auth_types.AccessTokenRequest()
    msg.access_token = data.access_token
    return msg


def form_access_token_resp(response):
    data = {
        "access_token": response.access_token,
        "token_type": response.token_type,
        "user": {
            "id": response.user.id,
            "name": response.user.name,
            "email": response.user.email,
            "photo": response.user.photo
        }
    }
    return serialize(data)


def form_login_msg(data):
    msg = auth_types.LoginRequest()
    msg.email = data.email
    msg.password = data.password
    return msg


def form_register_msg(data):
    msg = auth_types.RegisterRequest()
    msg.name = data.name
    msg.email = data.email
    msg.password = data.password
    return msg


def form_user_resp(response):
    data = {
        "id": response.id,
        "name": response.name,
        "email": response.email,
        "photo": None if response.photo == 0 else response.photo,
    }
    return serialize(data)


def form_delete_user_msg(data):
    msg = auth_types.DeleteUserRequest()
    msg.id = data.id
    return msg


def form_delete_user_resp(response):
    return serialize({
        "success": response.success
    })


def form_update_user_msg(user_id, name, photo):
    print(photo)
    msg = auth_types.UpdateUserRequest()
    msg.id = user_id
    msg.name = name
    if photo is not None:
        msg.photo = photo
    return msg
