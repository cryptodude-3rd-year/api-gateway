from typing import Annotated

from fastapi import APIRouter, File, HTTPException, Form, Depends
from fastapi.responses import JSONResponse

from auth_utils import login_required, auth_scheme
from microservices.auth.data_utils import (
    form_access_token_request_msg,
    form_access_token_resp,
    form_login_msg,
    form_register_msg,
    form_user_resp,
    form_delete_user_msg,
    form_delete_user_resp,
    form_update_user_msg,
)
from microservices.auth.schemas import (
    AccessTokenRequest,
    LoginRequest,
    RegisterRequest,
    DeleteUser,
)
from utils import create_stub

auth_router = APIRouter()


@auth_router.post("/get_access_token")
async def get_access_token(access_token: AccessTokenRequest):
    stub = create_stub("oauth")
    msg = form_access_token_request_msg(access_token)
    try:
        resp = stub.GetAccessToken(msg)
        fast_api_resp = form_access_token_resp(resp)
    except Exception as e:
        raise HTTPException(status_code=500)
    else:
        return JSONResponse(fast_api_resp)


@auth_router.post("/login")
async def login(user_to_login: LoginRequest):
    stub = create_stub("auth")
    msg = form_login_msg(user_to_login)
    try:
        resp = stub.Login(msg)
        fast_api_resp = form_access_token_resp(resp)
    except Exception as e:
        raise HTTPException(status_code=500)
    else:
        return JSONResponse(fast_api_resp)


@auth_router.post("/register")
async def register(user_to_register: RegisterRequest):
    stub = create_stub("auth")
    msg = form_register_msg(user_to_register)
    try:
        resp = stub.Register(msg)
        fast_api_resp = form_user_resp(resp)
    except Exception as e:
        raise HTTPException(status_code=500)
    else:
        return JSONResponse(fast_api_resp)


@auth_router.post("/delete_user")
@login_required
async def delete_user(token=Depends(auth_scheme), user_to_delete: DeleteUser = None):
    if user_to_delete is None:
        raise HTTPException(status_code=400)

    stub = create_stub("user_management")
    msg = form_delete_user_msg(user_to_delete)
    try:
        resp = stub.DeleteUser(msg)
        fast_api_resp = form_delete_user_resp(resp)
    except Exception as e:
        raise HTTPException(status_code=500)
    else:
        return JSONResponse(fast_api_resp)


@auth_router.post("/update_user")
@login_required
async def update_user(
        token=Depends(auth_scheme),
        photo: Annotated[bytes, File(...)] = None,
        name: str = Form(...),
        user_id: int = Form(...)
):
    stub = create_stub("user_management")
    msg = form_update_user_msg(user_id, name, photo)
    try:
        resp = stub.UpdateUser(msg)
        fast_api_resp = form_user_resp(resp)
    except Exception as e:
        raise HTTPException(status_code=500)
    else:
        return JSONResponse(fast_api_resp)
