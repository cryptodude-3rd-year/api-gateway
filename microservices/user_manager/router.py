from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import JSONResponse

from auth_utils import login_required, auth_scheme
from microservices.user_manager.data_utils import (
    form_filter_msg,
    form_create_currency_exchange_sub_msg,
    form_create_update_dashboard_msg,
    form_status_code_resp,
    form_currency_resp,
    form_exchange_resp,
    form_dashboard_resp,
    form_faqs_resp,
    form_support_question_resp,
    form_support_questions_resp,
    form_create_support_question_msg,
)
from microservices.user_manager.schemas import (
    CreateCurrSub,
    CreateExchangeSub,
    CreateDashboard,
    UpdateDashboard,
    CreateSupportQuestion,
)
from utils import create_stub
import generated_py_proto.user_manager.tech_support_pb2 as support_types

user_manager_router = APIRouter()


@user_manager_router.get("/currency_sub")
@login_required
async def get_currency_sub(
        token=Depends(auth_scheme),
        sub_id: int = None,
        curr_id: int = None,
        user_id: int = None
):
    stub = create_stub("currency_sub")
    msg = form_filter_msg("currency_sub_request", sub_id, user_id, currency_id=curr_id)
    try:
        resp = stub.GetCurrencySub(msg)
        data = form_currency_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(data)


@user_manager_router.post("/currency_sub")
@login_required
async def create_currency_sub(token=Depends(auth_scheme), curr_sub_data: CreateCurrSub = None):
    if curr_sub_data is None:
        raise HTTPException(status_code=400)

    stub = create_stub("currency_sub")
    msg = form_create_currency_exchange_sub_msg("currency_sub", curr_sub_data)
    try:
        resp = stub.CreateCurrencySub(msg)
        fast_api_resp = form_status_code_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.delete("/currency_sub")
@login_required
async def delete_currency_sub(
        token=Depends(auth_scheme),
        sub_id: int = None,
        curr_id: int = None,
        user_id: int = None
):
    stub = create_stub("currency_sub")
    msg = form_filter_msg("currency_sub_request", sub_id, user_id, currency_id=curr_id)
    try:
        resp = stub.DeleteCurrencySub(msg)
        fast_api_resp = form_status_code_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.get("/exchange_sub")
@login_required
async def get_exchange_sub(
        token=Depends(auth_scheme),
        sub_id: int = None,
        exchange_id: int = None,
        user_id: int = None
):
    stub = create_stub("exchange_sub")
    msg = form_filter_msg("exchange_sub_request", sub_id, user_id, exchange_id=exchange_id)
    try:
        resp = stub.GetExchangeSub(msg)
        data = form_exchange_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(data)


@user_manager_router.post("/exchange_sub")
@login_required
async def create_exchange_sub(token=Depends(auth_scheme), exchange_sub_data: CreateExchangeSub = None):
    if exchange_sub_data is None:
        raise HTTPException(status_code=400)

    stub = create_stub("exchange_sub")
    msg = form_create_currency_exchange_sub_msg("exchange_sub", exchange_sub_data)
    try:
        resp = stub.CreateExchangeSub(msg)
        fast_api_resp = form_status_code_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.delete("/exchange_sub")
@login_required
async def delete_exchange_sub(
        token=Depends(auth_scheme),
        sub_id: int = None,
        exchange_id: int = None,
        user_id: int = None
):
    stub = create_stub("exchange_sub")
    msg = form_filter_msg("exchange_sub_request", sub_id, user_id, exchange_id=exchange_id)
    try:
        resp = stub.DeleteExchangeSub(msg)
        fast_api_resp = form_status_code_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.get("/dashboard")
@login_required
async def get_dashboard(token=Depends(auth_scheme), dashboard_id: int = None, user_id: int = None):
    stub = create_stub("dashboard")
    msg = form_filter_msg("dashboard_filters", dashboard_id, user_id)
    try:
        resp = stub.GetDashboard(msg)
        fast_api_resp = form_dashboard_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.post("/dashboard")
@login_required
async def create_dashboard(token=Depends(auth_scheme), dashboard_data: CreateDashboard = None):
    if dashboard_data is None:
        raise HTTPException(status_code=400)

    stub = create_stub("dashboard")
    msg = form_create_update_dashboard_msg("dashboard_create", dashboard_data)
    try:
        resp = stub.CreateDashboard(msg)
        fast_api_resp = form_status_code_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.patch("/dashboard")
@login_required
async def update_dashboard(token=Depends(auth_scheme), dashboard_data: UpdateDashboard = None):
    if dashboard_data is None:
        raise HTTPException(status_code=400)

    stub = create_stub("dashboard")
    msg = form_create_update_dashboard_msg("dashboard_update", dashboard_data)
    try:
        resp = stub.UpdateDashboard(msg)
        fast_api_resp = form_status_code_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.delete("/dashboard")
@login_required
async def delete_dashboard(token=Depends(auth_scheme), dashboard_id: int = None, user_id: int = None):
    stub = create_stub("dashboard")
    msg = form_filter_msg("dashboard_filters", dashboard_id, user_id)
    try:
        resp = stub.DeleteDashboard(msg)
        fast_api_resp = form_status_code_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.get("/faq")
async def get_all_faqs():
    stub = create_stub("tech_support")
    msg = support_types.FAQRequest()
    try:
        resp = stub.GetAllFAQs(msg)
        fast_api_resp = form_faqs_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.get("/support/questions/{id}")
@login_required
async def get_support_question(token=Depends(auth_scheme), id: int = None):
    stub = create_stub("tech_support")
    msg = support_types.SingleTechSupportRequest()
    msg.id = id
    try:
        resp = stub.GetTechSupportQuestion(msg)
        fast_api_resp = form_support_question_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.get("/support/questions")
@login_required
async def get_users_support_questions(token=Depends(auth_scheme), user_id: int = None):
    if user_id is None:
        raise HTTPException(status_code=400, detail="No question id")

    stub = create_stub("tech_support")
    msg = support_types.MultiTechSupportRequest()
    msg.user_id = user_id
    try:
        resp = stub.GetTechSupportQuestions(msg)
        fast_api_resp = form_support_questions_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@user_manager_router.post("/support/questions")
@login_required
async def create_support_question(token=Depends(auth_scheme), question_data: CreateSupportQuestion = None):
    stub = create_stub("tech_support")
    msg = form_create_support_question_msg(question_data)
    try:
        resp = stub.CreateTechSupportQuestion(msg)
        fast_api_resp = form_support_question_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)

