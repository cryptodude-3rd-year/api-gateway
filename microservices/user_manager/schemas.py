from pydantic import BaseModel
from typing import List
from datetime import datetime


class CreateCurrSub(BaseModel):
    user_id: int
    currency_id: int


class CreateExchangeSub(BaseModel):
    user_id: int
    exchange_id: int


class CreateDashboard(BaseModel):
    user_id: int
    period_start_time: datetime = None
    period_end_time: datetime = None
    exchanges: List[int] = None
    buy_currencies: List[int] = None
    sell_currencies: List[int] = None


class UpdateDashboard(BaseModel):
    id: int
    period_start_time: datetime = None
    period_end_time: datetime = None
    exchanges: List[int] = None
    buy_currencies: List[int] = None
    sell_currencies: List[int] = None


class CreateSupportQuestion(BaseModel):
    user_id: int
    theme: str
    question: str
