import datetime as dt

from fastapi import status, HTTPException

import generated_py_proto.user_manager.subscription_pb2 as sub_types
import generated_py_proto.user_manager.dashboard_pb2 as dashboard_types
import generated_py_proto.user_manager.tech_support_pb2 as support_types
from serializers import serialize
from utils import form_datetime_field, form_repeated_field

filter_message_map = {
    "currency_sub_request": sub_types.CurrencySubRequest,
    "exchange_sub_request": sub_types.ExchangeSubRequest,
    "dashboard_filters": dashboard_types.DashboardFilters,
}

create_message_map = {
    "currency_sub": sub_types.CreateCurrencySubRequest,
    "exchange_sub": sub_types.CreateExchangeSubRequest,
    "dashboard_create": dashboard_types.CreateDashboardMsg,
    "dashboard_update": dashboard_types.UpdateDashboardMsg,
}


def form_filter_msg(msg_type, record_id, user_id, **kwargs):
    msg = filter_message_map[msg_type]()

    if record_id is not None:
        msg.id = record_id

    if user_id is not None:
        msg.user_id = user_id

    if msg_type == "currency_sub_request":
        currency_id = kwargs.get("currency_id", None)
        if currency_id is not None:
            msg.currency_id = currency_id
    elif msg_type == "exchange_sub_request":
        exchange_id = kwargs.get("exchange_id", None)
        if exchange_id is not None:
            msg.exchange_id = exchange_id

    return msg


def form_create_currency_exchange_sub_msg(sub_type, data):
    msg = create_message_map[sub_type]()
    msg.user_id = data.user_id

    if sub_type == "currency_sub":
        msg.currency_id = data.currency_id
    elif sub_type == "exchange_sub":
        msg.exchange_id = data.exchange_id

    return msg


def form_create_update_dashboard_msg(sub_type, data):
    msg = create_message_map[sub_type]()

    if sub_type == "dashboard_create":
        msg.user_id = data.user_id
    elif sub_type == "dashboard_update":
        msg.id = data.id

    if data.period_start_time is not None:
        msg.period_start_time = round(data.period_start_time.timestamp())

    if data.period_end_time is not None:
        msg.period_end_time = round(data.period_end_time.timestamp())

    if data.exchanges is not None:
        msg.exchanges.extend(data.exchanges)

    if data.buy_currencies is not None:
        msg.buy_currencies.extend(data.buy_currencies)

    if data.sell_currencies is not None:
        msg.sell_currencies.extend(data.sell_currencies)

    return msg


def form_status_code_resp(response):
    if response.status == 200:
        return status.HTTP_200_OK
    elif response.status == 400:
        raise HTTPException(status_code=400)
    elif response.status == 500:
        raise HTTPException(status_code=500)


def form_currency_resp(response):
    data = []
    for currency_sub in response.currency_subs:
        data.append({
            'id': currency_sub.id,
            'user_id': currency_sub.user_id,
            'currency_id': currency_sub.currency_id,
            'sub_time': dt.datetime.fromtimestamp(currency_sub.sub_time).strftime("%d.%m.%Y %H:%M"),
        })

    data = {"data": data}
    return serialize(data)


def form_exchange_resp(response):
    data = []
    for exchange_sub in response.exchange_subs:
        data.append({
            'id': exchange_sub.id,
            'user_id': exchange_sub.user_id,
            'currency_id': exchange_sub.exchange_id,
            'sub_time': dt.datetime.fromtimestamp(exchange_sub.sub_time).strftime("%d.%m.%Y %H:%M"),
        })

    data = {"data": data}
    return serialize(data)


def form_dashboard_resp(response):
    data = []
    for dashboard in response.dashboards:
        data.append({
            'id': dashboard.id,
            'user_id': dashboard.user_id,
            'period_start_time': form_datetime_field(dashboard.period_start_time),
            'period_end_time': form_datetime_field(dashboard.period_end_time),
            'exchanges': form_repeated_field(dashboard.exchanges),
            'buy_currencies': form_repeated_field(dashboard.buy_currencies),
            'sell_currencies': form_repeated_field(dashboard.sell_currencies),
            'created_time': form_datetime_field(dashboard.created_time),
            'updated_time': form_datetime_field(dashboard.updated_time),
        })

    data = {"data": data}
    return serialize(data)


def form_faqs_resp(response):
    data = []
    for faq in response.faqs:
        data.append({
            "id": faq.id,
            "theme": faq.theme,
            "question": faq.question,
            "answer": faq.answer,
            "created": form_datetime_field(faq.created),
            "updated": form_datetime_field(faq.updated),
        })
    return serialize({"faqs": data})


def create_support_question_dict(question):
    return {
        "id": question.id,
        "user_id": question.user_id,
        "theme": question.theme,
        "question": question.question,
        "answer": None if question.answer == "" else question.answer,
        "created": form_datetime_field(question.created),
        "updated": form_datetime_field(question.updated),
    }


def form_support_question_resp(response):
    return serialize(create_support_question_dict(response))


def form_support_questions_resp(response):
    data = []
    for question in response.questions:
        data.append(create_support_question_dict(question))
    return serialize({"questions": data})


def form_create_support_question_msg(data):
    msg = support_types.CreateTechSupportQuestionRequest()
    msg.user_id = data.user_id
    msg.theme = data.theme
    msg.question = data.question
    return msg
