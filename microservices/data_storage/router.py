from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import JSONResponse

from auth_utils import auth_scheme, login_required
from microservices.data_storage.data_utils import (
    form_get_all_curs_msg,
    form_multi_curr_resp,
    form_get_all_exchanges_msg,
    form_multi_exchange_resp,
)
from microservices.data_storage.utils import process_get_currency_by_id, process_get_exchange_by_id
from microservices.user_manager.data_utils import form_filter_msg
from utils import create_stub

data_storage_router = APIRouter()


@data_storage_router.get("/currencies")
async def get_currencies(include_inactive: bool = False):
    stub = create_stub("ds_currency")
    msg = form_get_all_curs_msg(include_inactive)
    try:
        resp = stub.GetAllCurrencies(msg)
        fast_api_resp = form_multi_curr_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@data_storage_router.get("/currencies/{currency_id}")
async def get_currency(currency_id: int):
    try:
        fast_api_resp = process_get_currency_by_id(currency_id)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@data_storage_router.get("/exchanges")
async def get_exchanges(include_inactive: bool = False):
    stub = create_stub("ds_exchange")
    msg = form_get_all_exchanges_msg(include_inactive)
    try:
        resp = stub.GetAllExchanges(msg)
        fast_api_resp = form_multi_exchange_resp(resp)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@data_storage_router.get("/exchanges/{exchange_id}")
async def get_exchange(exchange_id: int):
    try:
        fast_api_resp = process_get_exchange_by_id(exchange_id)
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500)
    return JSONResponse(fast_api_resp)


@data_storage_router.get("/subscriptions/currencies")
@login_required
async def get_subscriptions_currencies(token=Depends(auth_scheme), user_id: int = None):
    if user_id is not None:
        stub = create_stub("currency_sub")
        msg = form_filter_msg("currency_sub_request", record_id=None, user_id=user_id)
        try:
            resp = stub.GetCurrencySub(msg)
            currency_ids = [curr.currency_id for curr in resp.currency_subs]

            fast_api_resp = process_get_currency_by_id(currency_ids)
        except Exception as e:
            print(e)
            raise HTTPException(status_code=500)
        return JSONResponse(fast_api_resp)
    else:
        raise HTTPException(status_code=400, detail="No user id")


@data_storage_router.get("/subscriptions/exchanges")
@login_required
async def get_subscriptions_exchanges(token=Depends(auth_scheme), user_id: int = None):
    if user_id is not None:
        stub = create_stub("exchange_sub")
        msg = form_filter_msg("exchange_sub_request", record_id=None, user_id=user_id)
        try:
            resp = stub.GetExchangeSub(msg)
            exchange_ids = [ex.exchange_id for ex in resp.exchange_subs]

            fast_api_resp = process_get_exchange_by_id(exchange_ids)
        except Exception as e:
            print(e)
            raise HTTPException(status_code=500)
        return JSONResponse(fast_api_resp)
    else:
        raise HTTPException(status_code=400, detail="No user id")
