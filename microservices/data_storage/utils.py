from microservices.data_storage.data_utils import (
    form_multi_curr_resp,
    form_get_curr_msg,
    form_get_exchange_msg,
    form_multi_exchange_resp,
)
from utils import create_stub


def process_get_currency_by_id(request):
    stub = create_stub("ds_currency")
    msg = form_get_curr_msg(request)
    grpc_resp = stub.GetCurrenciesByIDs(msg)
    resp = form_multi_curr_resp(grpc_resp)
    return resp


def process_get_exchange_by_id(request):
    stub = create_stub("ds_exchange")
    msg = form_get_exchange_msg(request)
    grpc_resp = stub.GetExchangesByIDs(msg)
    resp = form_multi_exchange_resp(grpc_resp)
    return resp
