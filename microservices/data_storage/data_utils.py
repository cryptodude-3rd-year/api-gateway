import datetime as dt

import generated_py_proto.data_storage.data_storage_pb2 as ds_types
from serializers import serialize


def form_get_all_curs_msg(include_inactive):
    msg = ds_types.GetAllCurrenciesRequest()
    msg.include_inactive = include_inactive
    return msg


def form_multi_curr_resp(response):
    data = []
    for currency in response.currencies:
        data.append({
            "id": currency.id,
            "name": currency.name,
            "code_name": currency.code_name,
            "logo": currency.logo,
            "created_at": dt.datetime.fromtimestamp(currency.created_at.seconds),
            "updated_at": dt.datetime.fromtimestamp(currency.updated_at.seconds),
            "is_active": currency.is_active,
        })

    return serialize({"currencies": data})


def form_get_curr_msg(curr_id):
    msg = ds_types.GetCurrenciesByIDsRequest()
    if not isinstance(curr_id, list):
        msg.ids.extend([curr_id])
    else:
        msg.ids.extend(curr_id)
    return msg


def form_get_all_exchanges_msg(include_inactive):
    msg = ds_types.GetAllExchangesRequest()
    msg.include_inactive = include_inactive
    return msg


def form_multi_exchange_resp(response):
    data = []
    for exchange in response.exchanges:
        data.append({
            "id": exchange.id,
            "name": exchange.name,
            "url": exchange.url,
            "logo": exchange.logo,
            "maker_fee": exchange.maker_fee,
            "taker_fee": exchange.taker_fee,
            "fee_url": exchange.fee_url,
            "created_at": dt.datetime.fromtimestamp(exchange.created_at.seconds),
            "updated_at": dt.datetime.fromtimestamp(exchange.updated_at.seconds),
            "is_active": exchange.is_active,
        })

    return serialize({"exchanges": data})


def form_get_exchange_msg(exchange_id):
    msg = ds_types.GetExchangesByIDsRequest()
    if not isinstance(exchange_id, list):
        msg.ids.extend([exchange_id])
    else:
        msg.ids.extend(exchange_id)
    return msg
