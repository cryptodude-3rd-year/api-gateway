import asyncio

from fastapi import APIRouter, WebSocket

from microservices.statistic_gen.data_utils import form_hot_prices_resp
from serializers import serialize
from utils import create_stub
from generated_py_proto.statistic_gen import statistic_generation_pb2 as stat_types

statistic_gen_router = APIRouter()


@statistic_gen_router.websocket("/hot_prices")
async def get_hot_prices(websocket: WebSocket):
    await websocket.accept()
    while True:
        stub = create_stub("statistic")
        msg = stat_types.GetHotPricesParams()
        try:
            resp = stub.GetHotPrices(msg)
            fast_api_resp = form_hot_prices_resp(resp)
        except Exception as e:
            print(e)
            fast_api_resp = serialize({})
        await websocket.send_json(fast_api_resp)
        await asyncio.sleep(600)
