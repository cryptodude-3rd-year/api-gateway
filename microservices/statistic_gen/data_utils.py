import datetime as dt

from serializers import serialize


def form_hot_prices_resp(response):
    data = []
    for hot_price in response.hot_prices:
        data.append({
            "id": hot_price.id,
            "sell_currency_id": hot_price.sell_currency_id,
            "buy_currency_id": hot_price.buy_currency_id,
            "exchange_id": hot_price.exchange_id,
            "price": hot_price.price,
            "time": dt.datetime.fromtimestamp(hot_price.time.seconds),
            "sell_currency": {
                "id": hot_price.sell_currency.id,
                "name": hot_price.sell_currency.name,
                "code_name": hot_price.sell_currency.code_name,
                "logo": hot_price.sell_currency.logo,
                "created_at": dt.datetime.fromtimestamp(hot_price.sell_currency.created_at.seconds),
                "updated_at": dt.datetime.fromtimestamp(hot_price.sell_currency.updated_at.seconds),
                "is_active": hot_price.sell_currency.is_active,
            },
            "buy_currency": {
                "id": hot_price.buy_currency.id,
                "name": hot_price.buy_currency.name,
                "code_name": hot_price.buy_currency.code_name,
                "logo": hot_price.buy_currency.logo,
                "created_at": dt.datetime.fromtimestamp(hot_price.buy_currency.created_at.seconds),
                "updated_at": dt.datetime.fromtimestamp(hot_price.buy_currency.updated_at.seconds),
                "is_active": hot_price.buy_currency.is_active,
            },
            "exchange": {
                "id": hot_price.exchange.id,
                "name": hot_price.exchange.name,
                "url": hot_price.exchange.url,
                "logo": hot_price.exchange.logo,
                "maker_fee": hot_price.exchange.maker_fee,
                "taker_fee": hot_price.exchange.taker_fee,
                "fee_url": hot_price.exchange.fee_url,
                "created_at": dt.datetime.fromtimestamp(hot_price.exchange.created_at.seconds),
                "updated_at": dt.datetime.fromtimestamp(hot_price.exchange.updated_at.seconds),
                "is_active": hot_price.exchange.is_active,
            },
            "delta": hot_price.delta,
        })

    return serialize({"hot_prices": data})
