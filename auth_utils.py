import datetime
from functools import wraps

import jose.exceptions
from fastapi import HTTPException
from fastapi.security import HTTPBearer
from jose import jwt

from config.constants import SECRET_KEY, ALGORITHM

auth_scheme = HTTPBearer()


def login_required(f):
    @wraps(f)
    async def wrapper(token, *args, **kwargs):
        token = token.credentials

        try:
            payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        except jose.exceptions.JWTError:
            raise HTTPException(status_code=403)

        now_timestamp = round(datetime.datetime.now(tz=datetime.timezone.utc).timestamp())

        if now_timestamp >= payload["exp"]:
            raise HTTPException(status_code=403)

        return await f(token, *args, **kwargs)
    return wrapper

