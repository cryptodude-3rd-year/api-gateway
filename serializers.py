from fastapi.encoders import jsonable_encoder


def serialize(data):
    return jsonable_encoder(data)
