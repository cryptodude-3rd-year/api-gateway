import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from config.constants import (
    APP_PORT,
    APP_HOST,
    USER_MANAGER_PREFIX,
    AUTH_PREFIX,
    DATA_STORAGE_PREFIX,
    STATISTIC_GEN_PREFIX,
    USER_MANAGER_ROUTER_TAG,
    AUTH_ROUTER_TAG,
    DATA_STORAGE_ROUTER_TAG,
    STATISTIC_GEN_ROUTER_TAG,
    CORS_ORIGINS,
)
from metrics import PrometheusMiddleware, metrics
from microservices.auth.router import auth_router
from microservices.data_storage.router import data_storage_router
from microservices.statistic_gen.router import statistic_gen_router
from microservices.user_manager.router import user_manager_router

app = FastAPI()

app.include_router(user_manager_router, prefix=USER_MANAGER_PREFIX, tags=[USER_MANAGER_ROUTER_TAG])
app.include_router(auth_router, prefix=AUTH_PREFIX, tags=[AUTH_ROUTER_TAG])
app.include_router(data_storage_router, prefix=DATA_STORAGE_PREFIX, tags=[DATA_STORAGE_ROUTER_TAG])
app.include_router(statistic_gen_router, prefix=STATISTIC_GEN_PREFIX, tags=[STATISTIC_GEN_ROUTER_TAG])


app.add_middleware(
    CORSMiddleware,
    allow_origins=CORS_ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.add_middleware(PrometheusMiddleware)
app.add_route('/metrics', metrics)


if __name__ == '__main__':
    uvicorn.run(app="app:app", host=APP_HOST, port=APP_PORT)
