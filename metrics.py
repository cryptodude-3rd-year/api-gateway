import time

from prometheus_client import CONTENT_TYPE_LATEST, REGISTRY, Counter, Histogram, generate_latest
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.requests import Request
from starlette.responses import Response
from starlette.routing import Match
from starlette.status import HTTP_500_INTERNAL_SERVER_ERROR


REQUESTS = Counter(
    'requests_total', 'Total count of requests by method and path', ['method', 'path']
)
RESPONSES = Counter(
    'responses_total',
    'Total count of responses by method, path and status codes',
    ['method', 'path', 'status'],
)
REQUESTS_PROCESSING_TIME = Histogram(
    'requests_processing_time_seconds',
    'Histogram of requests processing time by path (in seconds)',
    ['method', 'path'],
)


class PrometheusMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next: RequestResponseEndpoint) -> Response:
        method = request.method
        path = self.get_path_template(request)
        REQUESTS.labels(method=method, path=path).inc()
        before_time = time.perf_counter()
        try:
            response = await call_next(request)
        except BaseException as e:
            status = HTTP_500_INTERNAL_SERVER_ERROR
            RESPONSES.labels(method=method, path=path, status=status).inc()
            raise e
        else:
            status = response.status_code
            after_time = time.perf_counter()
            observe_time = after_time - before_time
            REQUESTS_PROCESSING_TIME.labels(method=method, path=path).observe(observe_time)
            RESPONSES.labels(method=method, path=path, status=status).inc()

        return response

    @staticmethod
    def get_path_template(request: Request) -> str:
        for route in request.app.routes:
            match, child_scope = route.matches(request.scope)
            if match == Match.FULL:
                return route.path
        return '/unhandled_paths'


def metrics(request: Request) -> Response:
    return Response(generate_latest(REGISTRY), headers={'Content-Type': CONTENT_TYPE_LATEST})
